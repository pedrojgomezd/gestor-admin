const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.less('resources/assets/less/main.less', 'public/less/app.css');
	//.js('resources/assets/js/app.js', 'public/compilar/js')
   //.sass('resources/assets/sass/app.scss', 'public/compilat/css')
