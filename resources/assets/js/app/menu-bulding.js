$('.dd').nestable({ /* config options */ });

$('.dd').on('change', function(){
	console.log(JSON.stringify($('.dd').nestable('serialize')));
})

$('#add-menu-name').keyup(function(){
	console.log(this.value)
	btn = $('#add-menu-btn');
	if (this.value == '') {
		btn.attr('disabled', 'disabled');
	}else{
		btn.removeAttr('disabled');		
	}
})

$('#add-menu-btn').click(function(){
	menu = $('#add-menu-form').serialize();
	axios.post('/admin/menus', menu).then(function(data){
		$('#add-menu').modal('toggle');
		$('#add-menu-name').val('');
		$('#add-menu-btn').attr('disabled', 'disabled');
	}).catch(function(error){
		console.log(error);
	})
})