<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <a class="h5 text-white" href="index.html">
                    <i class="fa fa-eye text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide">AdminGastor</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">

                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Recepcion</span></li>

                    <li>
                        <a href="{{ route('users') }}"><i class="fa fa-dashboard"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    
                    <li>
                        <a href="{{ route('clients.index') }}"><i class="si si-users"></i><span class="sidebar-mini-hide">Clientes</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="si si-wrench"></i><span class="sidebar-mini-hide">Ordenes</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="si si-drawer"></i><span class="sidebar-mini-hide">Facturas</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Productos</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Solicitudes</span></a>
                    </li>

                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Administrador</span></li>
                    
                    <li>
                        <a href="{{ route('users') }}"><i class="si si-users"></i><span class="sidebar-mini-hide">Usuarios</span></a>
                    </li>

                    <li>
                        <a href="{{ route('users') }}"><i class="si si-users"></i><span class="sidebar-mini-hide">Empresas</span></a>
                    </li>

                    <li>
                        <a href="{{ route('users') }}"><i class="si si-users"></i><span class="sidebar-mini-hide">Inventario</span></a>
                    </li>

                    <li>
                        <a href="{{ route('users') }}"><i class="si si-users"></i><span class="sidebar-mini-hide">Sucursales</span></a>
                    </li>

                    <li>
                        <a href="{{ route('users') }}"><i class="si si-users"></i><span class="sidebar-mini-hide">Marcas</span></a>
                    </li>
                    
                    <li>
                        <a href="{{ asset('admin/menus') }}"><i class="fa fa-bars"></i><span class="sidebar-mini-hide">Menus</span></a>
                    </li> 
                    
                    <li>
                        <a href="{{ asset('admin/medias') }}"><i class="si si-social-dropbox"></i><span class="sidebar-mini-hide">Media</span></a>
                    </li>

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>