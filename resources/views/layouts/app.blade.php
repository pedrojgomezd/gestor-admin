<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('less/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nestable.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app"></div>
    <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
        
        @include('layouts.aside')

        @include('layouts.menu')

        @include('layouts.nav')
        
        <main id="main-container">
            <!-- Page Header -->
            <div class="content bg-gray-lighter">
                <div class="row items-push">
                    <div class="col-sm-7">
                        <h1 class="page-heading">
                            @yield('page-heading')
                        </h1>                        
                    </div>
                    <div class="col-sm-5 text-right hidden-xs">
                        <ol class="breadcrumb push-10-t">
                            @yield('breadcrumb')
                        </ol>
                    </div>
                </div>
            </div>
            <!-- END Page Header -->

            <!-- Page Content -->
            <div class="content">
                @yield('content')
            </div>
            <!-- END Page Content -->
        </main>

        @include('layouts.footer')
    </div>

    @include('layouts.modal')
    <!-- Scripts -->
    <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="{{ asset('compilar/js/app.js') }}"></script>
        <script src="{{ asset('js/core/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('js/core/jquery.scrollLock.min.js') }}"></script>
        <script src="{{ asset('js/core/jquery.appear.min.js') }}"></script>
        <script src="{{ asset('js/core/jquery.countTo.min.js') }}"></script>
        <script src="{{ asset('js/core/jquery.placeholder.min.js') }}"></script>
        <script src="{{ asset('js/core/js.cookie.min.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>      
</body>
</html>
