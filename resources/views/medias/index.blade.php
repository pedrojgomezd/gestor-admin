@extends('layouts.app')

@section('page-heading')
<i class="si si-social-dropbox"></i> Medias 
@endsection

@section('content')
<div class="block block-themed block-rounded block-bordered">
    <div class="block-header bg-gray-light">
    	<div class="block-options-simple block-options-simple-left">
            <div class="btn-group">
            	<a href="#" class="btn btn-primary btn-sm"><i class="si si-cloud-upload"></i> Subir</a>
            	<a href="#" class="btn btn-default btn-sm"><i class="si si-folder"></i> Nueva Carpeta</a>
            </div>
			<div class="btn-group">
				<a href="" class="btn btn-info btn-sm"><i class="si si-reload"></i></a>
			</div>
			<div class="btn-group">
				<a href="" class="btn btn-default btn-sm"><i class="fa fa-chain-broken"></i> Renombrar</a>
				<a href="" class="btn btn-danger btn-sm"><i class="si si-trash"></i></a>
			</div> 
			<div class="btn-group">
				<a href="" class="btn btn-link btn-sm"> Raiz /</a> 
				<a href="" class="btn btn-link btn-sm">Videos</a>
			</div>    		
    	</div>
    </div>
    <div class="block-content" id="medias">
    	<div class="row">
        @for($i = 0; $i < 8; $i++)
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="javascript:void(0)">
                    <div class="block-content block-content-full">
                        <i class="si si-folder fa-4x text-primary-darker"></i>
                        <div class="font-w600 push-15-t">Songs</div>
                        <p>53kb</p>
                    </div>
                </a>
            </div>
        @endfor
    	</div>
    </div>
</div>
@endsection