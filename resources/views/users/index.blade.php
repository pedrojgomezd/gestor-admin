@extends('layouts.app')

@section('page-heading')
<i class="si si-users"></i> Usuarios 
<a data-toggle="modal" href='#add-user-modal' class="btn btn-success">
	<i class="si si-plus"></i> Crear
</a>
@endsection

@section('breadcrumb')
	<li>Dashboard</li>
	<li>
		<a href="#" class="link-effect">Usuarios</a>
	</li>
@endsection

@section('content')

	<div class="block block-default">
		<div class="block-header bg-primary">
			<h3 class="block-title"><i class="fa fa-list"></i> Usuarios</h3>
		</div>
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>Identificacion</th>
					<th>Nombre</th>
					<th>Email</th>
					<th>Telefono</th>
					<th>Roll</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>19.747.349</td>
					<td>Pedro J Gomez</td>
					<td>PedroJGomez@gmail.com</td>
					<td>0414-6121298</td>
					<td>Administrador</td>
					<td>
						<a href="#" class="btn btn-primary btn-square btn-sm"><i class="fa fa-eye"></i> Ver</a>
						<a href="#" class="btn btn-success btn-square btn-sm"><i class="fa fa-edit"></i> Editar</a>
						<a href="#" class="btn btn-danger btn-square btn-sm"><i class="fa fa-trash"></<i></i></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
@include('users.add-user')
@endsection