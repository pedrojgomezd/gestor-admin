<div class="modal fade" id="add-user-modal">
	<form action="{{ route('add') }}" method="post">
		{{ csrf_field() }}
		<div class="modal-dialog modal-dialog-popin modal-lg">
			<div class="modal-content">
				<div class="block block-themed block-transparent">
					<div class="block-header bg-modern">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title"><i class="fa fa-plus-square-o"></i> Nuevo Usuario en el Sistema.</h3>
					</div>
		
					<div class="block-content">
						<div class="row">
							<div class="col-md-4">
								<label for="">Identificacion.</label>
								<div class="input-group">
	                                <span class="input-group-addon"><i class="fa fa-address-card-o"></i></span>
	                                <input class="form-control" type="text" id="title" name="title" placeholder="Identificacion.">
	                            </div>
							</div>
							<div class="col-md-4">
								<label for="">Nombre y Apellido.</label>
								<div class="input-group">
	                                <span class="input-group-addon"><i class="si si-user"></i></span>
	                                <input class="form-control" type="text" id="title" name="title" placeholder="Nombre y Apellidp.">
		                        </div>
							</div>
							<div class="col-md-4">
								<label for="">Roll.</label>
								<div class="input-group">
	                                <span class="input-group-addon"><i class="si si-speedometer"></i></span>
	                                <select name="roll" id="roll" class="form-control">
	                                	<option value="">Roll</option>
	                                </select>
		                        </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="">Telefono.</label>
								<div class="input-group">
	                                <span class="input-group-addon"><i class="si si-screen-smartphone"></i></span>
	                                <input class="form-control" type="text" id="phone" name="phone" placeholder="(000)-000-0000">
		                        </div>
							</div>
							<div class="col-md-4">
								<label for="">Email.</label>
								<div class="input-group">
	                                <span class="input-group-addon"><i class="si si-envelope"></i></span>
	                                <input class="form-control" type="email" id="email" name="email" placeholder="ejemplo@ejemplo.com">
		                        </div>
							</div>
							<div class="col-md-4">
								<label for="">Contraseña.</label>
								<div class="input-group">
	                                <span class="input-group-addon"><i class="si si-key"></i></span>
	                                <input class="form-control" type="password" id="password" name="password" placeholder="Password">
		                        </div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-sm btn-primary" type="submit" ><i class="fa fa-check"></i> Guardar</button>
				</div>
			</div>
		</div>
	</form>
</div>