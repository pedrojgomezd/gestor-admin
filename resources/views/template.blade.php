<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Template Admin Gastor</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('less/app.css') }}">
</head>
<body>

	<div id="page-container">
		<div id="sidebar">
			<div class="sidebar-content">
				<div class="side-content">
					<ul class="nav-main">
						<li class="nav-main-heading"><span class="sidebar-mini-hide">Recepcion</span></li>

		                <li>
		                    <a href="{{ route('users') }}"><i class="fa fa-dashboard"></i><span class="sidebar-mini-hide">Dashboard</span></a>
		                </li>

					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<script src="{{ asset('compilar/js/app.js') }}"></script>
</body>
</html>