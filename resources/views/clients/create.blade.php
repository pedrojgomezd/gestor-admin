@extends('layouts.app')

@section('page-heading')
<i class="si si-users"></i> Nuevo Clientes 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<h3 class="block-title">Datos del CLiente</h3>
	</div>
	<div class="block-content">
		
        <div class="row">
			<div class="col-lg-6">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-secondary" type="button">Go!</button>
					</span>
					<input type="text" class="form-control" placeholder="Search for...">
				</div>
			</div>
			<div class="col-lg-6">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-secondary" type="button">Go!</button>
					</span>
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection