@extends('layouts.app')

@section('page-heading')
<i class="si si-users"></i> Clientes 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<div class="block-options-simple btn-group btn-group-xs">
			<a href="{{ route('client.create') }}" class="btn btn-default"><i class="si si-plus"></i> Crear</a>
		</div>
		<h3 class="block-title">Primary</h3>
	</div>
	<div class="">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
@endsection