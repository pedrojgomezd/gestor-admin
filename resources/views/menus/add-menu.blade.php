<div class="modal fade" id="add-menu">

	<form action="{{ asset('admin/menu') }}" method="post" id="add-menu-form">
		<div class="modal-dialog modal-dialog-popin modal-sm">
			<div class="modal-content">
				<div class="block block-themed block-transparent">
					<div class="block-header bg-modern">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title"><i class="fa fa-plus-square-o"></i> Nuevo Menu.</h3>
					</div>
		
					<div class="block-content">
						<label for="">Nombre del Menu</label>
						<input type="text" class="form-control" id="add-menu-name" name="name" placeholder="Nombre de menu.">
						<hr>
					</div>
		
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-sm btn-primary" type="button" id="add-menu-btn" disabled><i class="fa fa-check"></i> Guardar</button>
				</div>
			</div>
		</div>
	</form>

</div>