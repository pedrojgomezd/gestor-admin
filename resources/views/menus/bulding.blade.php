@extends('layouts.app')

@section('page-heading')
<i class="si si-list"></i >Creador 
<a class="btn btn-success btn-sm" data-toggle="modal" href='#modal-id'><i class="si si-plus"></i> Agrega Item</a>
@endsection

@section('breadcrumb')
	<li>Dashboard</li>
    <li><a class="link-effect" href="">Menu</a></li>
    <li>Creador</li>
@endsection

@section('content')
<div class="row">
	<div class="block block-default">
        <div class="block-header bg-gray-lighter">
            <h3 class="block-title">Items del Menu <i>"{{$menu->name}}"</i></h3>
        </div>
        <div class="block-content" style="padding: 20px; display: flex;">
            <div class="dd">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="pull-right item_actions">
				            <div class="btn-sm btn-danger pull-right delete" data-id="1">
				                <i class="voyager-trash"></i> Delete
				            </div>
				            <div class="btn-sm btn-primary pull-right edit" data-id="1" data-title="Dashboard" data-url="/admin" data-target="_self" data-icon_class="voyager-boat" data-color="#1d4a9e" data-route="" data-parameters="null">
				                <i class="voyager-edit"></i> Edit
				            </div>
			        	</div>
						<div class="dd-handle"><i class="fa fa-tachometer"></i> <span>Dashboard</span> <small class="url">/admin</small></div>
					</li>
					<li class="dd-item" data-id="2">
						<div class="pull-right item_actions">
				            <div class="btn-sm btn-danger pull-right delete" data-id="1">
				                <i class="voyager-trash"></i> Delete
				            </div>
				            <div class="btn-sm btn-primary pull-right edit" data-id="1" data-title="Dashboard" data-url="/admin" data-target="_self" data-icon_class="voyager-boat" data-color="#1d4a9e" data-route="" data-parameters="null">
				                <i class="voyager-edit"></i> Edit
				            </div>
			        	</div>
						<div class="dd-handle"><i class="fa fa-tachometer"></i> <span>Dashboard</span> <small class="url">/admin</small></div>
					</li>					
				</ol>
			</div>
        </div>
    </div>
</div>

@include('menus.add-item')

@endsection