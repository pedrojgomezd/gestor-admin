<div class="modal fade" id="modal-id">
	<form action="{{ route('add') }}" method="post">
		{{ csrf_field() }}
		<div class="modal-dialog modal-dialog-popin">
			<div class="modal-content">
				<div class="block block-themed block-transparent">
					<div class="block-header bg-modern">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title"><i class="fa fa-plus-square-o"></i> Nuevo Item en el menu.</h3>
					</div>
		
					<div class="block-content">
						<div class="row">
							<div class="col-md-6">
								<label for="">Titulo del intem en el menu.</label>
								<div class="input-group">
		                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
		                                <input class="form-control" type="text" id="title" name="title" placeholder="Titulo.">
		                            </div>
							</div>
							<div class="col-md-6">
								<label for="">URL para el item en el menu.</label>
								<div class="input-group">
		                                <span class="input-group-addon"><i class="fa fa-minus"></i></span>
		                                <input class="form-control" type="text" id="url" name="url" placeholder="URL">
		                            </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="">Icono del Item.</label>
								<div class="input-group">
		                                <span class="input-group-addon"><i class="fa fa-font-awesome"></i></span>
		                                <input class="form-control" type="text" id="icon_class" name="icon_class" placeholder="fa-users">
		                            </div>
							</div>
							<div class="col-md-4">
								<label for="">Color.</label>
								<div class="input-group">
		                                <div class="js-colorpicker input-group colorpicker-element">
		                                    <input class="form-control" type="text" id="color" name="color" value="#5c90d2">
		                                    <span class="input-group-addon"><i style="background-color: rgb(92, 144, 210);"></i></span>
		                                </div>
		                            </div>
							</div>
							<div class="col-md-4">
								<label for="">Target (Donde abrir).</label>
								<div class="input-group">
		                                <span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
		                                <select name="target" id="target" class="form-control">
		                                	<option value="_black">_black</option>
		                                	<option value="_self">_self</option>
		                                </select>
		                            </div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-sm btn-primary" type="submit" ><i class="fa fa-check"></i> Guardar</button>
				</div>
			</div>
		</div>
	</form>
</div>