	@extends('layouts.app')

@section('page-heading')
<i class="si si-list"></i> Menus <a class="btn btn-success btn-sm" data-toggle="modal" href='#add-menu'><i class="si si-plus"></i> Crear Menu</a>
@endsection

@section('breadcrumb')
	<li>Menus</li>
@endsection

@section('content')

<div class="block block-default block-rounded">
	<div class="block-header bg-gray">
		<h3 class="block-title">
			Menus
		</h3>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
		@foreach($menus as $menu)
			<tr>
				<td>{{ $menu->name }}</td>
				<td class="text-right">
					<a href="{{ asset('admin/menus/') }}/{{ $menu->id }}/builder" class="btn btn-primary btn-square btn-sm"><i class="fa fa-list"></i> Contruir</a>
					<button class="btn btn-success btn-square btn-sm"><i class="fa fa-edit"></i> Edit</button>
					<button class="btn btn-danger btn-square btn-sm"><i class="fa fa-trash"></i></button>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

@include('menus.add-menu')

@endsection