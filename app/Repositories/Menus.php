<?php 
namespace App\Repositories;

use App\Models\Menu;
use App\Models\MenuItem;

/**
* Menus repositories
*/
class Menus
{
	public $menu;
	public $menuItem;

	function __construct(Menu $menu, MenuItem $menuItem)
	{
		$this->menu = $menu;
		$this->menuItem = $menuItem;
	}

	public function find($menu)
	{
		return $this->menu->find($menu);
	}

	public function all()
	{
		return $this->menu->all();
	}

	public function add($request)
	{
		$menu = $this->menu->create($request);
		dd($menu);
	}

}