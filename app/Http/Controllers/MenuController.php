<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Menus;
use App\Models\Menu;

class MenuController extends Controller
{
	public $menus;
	function __construct(Menus $menus)
	{
		$this->menus = $menus;
	}
	public function index()
	{
		$menus = $this->menus->all();
    	return view('menus.index', compact('menus'));
	}

	public function create(Request $request)
	{
		$newMenu = $this->menus->add($request->all());
		return (string) $newMenu;
	}

    public function builder($menu)
    {	$menu = $this->menus->find($menu);
    	return view('menus.bulding', compact('menu'));
    }

    public function add_item(Request $request)
    {
    	//$this->menus->add_item($reqeust->all());
    	return $request->all();
    }
}
