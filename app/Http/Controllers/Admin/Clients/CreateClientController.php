<?php

namespace App\Http\Controllers\Admin\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CreateClientController extends Controller
{
    public function create()
    {
    	return view('clients.create');
    }
}
