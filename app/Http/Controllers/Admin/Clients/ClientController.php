<?php

namespace App\Http\Controllers\Admin\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
	Su funcionabilidad es
	listar los clientes
	y las vistas
 */

class ClientController extends Controller
{
    public function index()
    {
    	return view('clients.index');
    }
}
