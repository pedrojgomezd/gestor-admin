<?php 

//CLientes

Route::get('clients',[
        'uses' => 'Admin\Clients\ClientController@index',
        'as' =>'clients.index'
]);

Route::get('clients/create',[
        'uses' => 'Admin\Clients\CreateClientController@create',
        'as' =>'client.create'
]);


//Route Usuarios
Route::group(['as' => 'users','prefix' => 'users'], function() {
    Route::get('/', 'UsersController@index');
});
//Profiles
Route::group(['prefix' => 'profile'], function() {
    Route::get('/', function() {
        return view('profiles.index');
    });
});
//Route Menus
Route::group(['prefix' => 'menus'], function() {
    Route::get('/', 'MenuController@index');
    Route::post('/', 'MenuController@create');
    Route::get('{menu}/builder', 'MenuController@builder');
    // Route Menus > item
    Route::group(['prefix' => 'item'], function() {
        Route::delete('{id}', 	['uses' => 'MenuController@delete_menu', 'as' => 'destroy']);
        Route::post('/', 		['uses' => 'MenuController@add_item', 'as' => 'add']);
        Route::put('/', 		['uses' => 'MenuController@update_item', 'as' => 'update']);
    });
});

Route::group(['prefix' => 'medias'], function() {
    Route::get('/', function() {
        return view('medias.index');
    });
});
